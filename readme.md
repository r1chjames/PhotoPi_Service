PhotoPi Dropwizard application
====================================

Running the application
-----------------------
To run the application from within your IDE, start from **Application**.java with the following command line arguments:
**server** *path_to_config_file*.yml

About the application
---------------------
TO BE COMPLETED


Test page
----------------------
This  application starts up a Dropwizard instance on **localhost port 8080** with two resources serving the text
*Application is running...*. The endpoint can be accessed at the following resource (when the application is running):
*http://localhost:8080/{env}/healthcheck/health*

How to build the app
-----------------------
gradlew clean build will create a jar
