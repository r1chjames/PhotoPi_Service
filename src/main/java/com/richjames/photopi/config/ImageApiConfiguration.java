package com.richjames.photopi.config;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

/**
 * Created by SYSDEVAN on 01/08/2016.
 */
@Getter
@Setter
public class ImageApiConfiguration {

    @NotNull
    @JsonProperty("serviceEndpoint")
    private String serviceEndpoint;

    @NotNull
    @JsonProperty("apikey")
    private String apikey;

    @NotNull
    @JsonProperty("downloadLocation")
    private String downloadLocation;

}
