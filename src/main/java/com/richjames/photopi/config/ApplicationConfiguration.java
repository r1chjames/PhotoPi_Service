package com.richjames.photopi.config;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;
import lombok.Getter;

import javax.validation.constraints.NotNull;

@Getter
public class ApplicationConfiguration extends Configuration {

    @NotNull
    @JsonProperty
    private FlickrApiConfiguration flickrAPI = new FlickrApiConfiguration();

    @NotNull
    @JsonProperty
    private Px500ApiConfiguration px500API = new Px500ApiConfiguration();

    @NotNull
    @JsonProperty
    private MongoConfiguration mongo = new MongoConfiguration();

}
