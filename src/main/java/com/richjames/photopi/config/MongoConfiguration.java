package com.richjames.photopi.config;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

/**
 * Created by SYSDEVAN on 01/08/2016.
 */
@Getter
@Setter
public class MongoConfiguration {

    @NotNull
    @JsonProperty("host")
    private String host;

    @NotNull
    @JsonProperty("port")
    private int port;

}
