package com.richjames.photopi.utils;

import com.diffplug.common.base.Errors;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.richjames.photopi.exceptions.ServiceApplicationException;
import lombok.experimental.UtilityClass;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.StatusLine;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Response;
import java.util.function.Supplier;

import static org.apache.http.protocol.HTTP.USER_AGENT;

@UtilityClass
public class HttpUtil {
  private static final Logger log = LoggerFactory.getLogger(HttpUtil.class);

  public static final Header USER_AGENT_HEADER = new BasicHeader("User-Agent", USER_AGENT);
  public static final Header JSON_HEADER = new BasicHeader("Content-type", "application/json");

  /**
   * Helper wrapping Response Supplier execution with PointServiceApplicationException rethrower
   *
   * @param supplier
   * @return Response
   * @throws ServiceApplicationException
   */
  public static Response withExceptionHandler(Supplier<Response> supplier) {
    try {
      return supplier.get();
    } catch (ServiceApplicationException e) {
      log.error("Operation failed.", e);
      throw new ServiceApplicationException(e.getMessage(), e.getCause(), e.getReturnCode(), e.getErrorSequence());
    //} catch (JsonProcessingException e) {
      //log.error("Failed to process response JSON.", e);
      //throw new ServiceApplicationException(e.getMessage(), e.getCause(), 500, null);
    } catch (Throwable e) {
      log.error("Operation failed.", e);
      throw new ServiceApplicationException(e.getMessage(), e.getCause(), 500, null);
    }
  }

  public static String fromEntity(HttpEntity entity) {
    return Errors.rethrow().get(() -> EntityUtils.toString(entity));
  }

  /**
   * Any 2xx code means success.
   *
   * @param status
   * @return true for any 2xx code, false otherwise
   */
  public static boolean isSuccess(StatusLine status) {
    return status.getStatusCode() / 100 == 2;
  }
}
