package com.richjames.photopi.utils;

/**
 * Created by peter on 23/10/2016.
 */
public class LogFormatters {

    /**
     * action, time in millis
     */
    public static final String LOG_ACTION_TIME = "%s %s millis";

}
