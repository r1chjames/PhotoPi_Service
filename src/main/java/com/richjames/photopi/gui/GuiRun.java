package com.richjames.photopi.gui;

import com.google.common.jimfs.Configuration;
import com.google.common.jimfs.Jimfs;
import org.apache.commons.io.FilenameUtils;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.file.FileSystem;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static java.nio.file.StandardOpenOption.CREATE;

public class GuiRun extends Canvas{

    private FileSystem fs;
    private Path photoDir;

    public static void main() {
        GuiRun m = new GuiRun();
        JFrame frame = new JFrame();
        frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
        frame.setUndecorated(true);
        frame.setVisible(true);
        frame.add(m);
    }

    public void paint(Graphics graphics) {
        try {
            URL imageLocation = new URL("https://drscdn.500px.org/photo/207838385/m%3D2048_k%3D1_a%3D1/e7053d5630355c8c8f53e3e9ff7550f2");
            createInMemoryFs();

            if (!imageExistInCache(imageLocation)) {
                addImageToCache(imageLocation);
            }

            BufferedImage bufferedImage = loadImageFromCache(imageLocation);
            drawImagefromPath(graphics, bufferedImage);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void createInMemoryFs() throws IOException {
        fs = Jimfs.newFileSystem(Configuration.unix());
        photoDir = fs.getPath("photos");
        Files.createDirectory(photoDir);
    }

    private void drawImagefromUrl(Graphics graphics, URL imageUrl) {
        Image image = null;
        try {
            image = ImageIO.read(imageUrl);
        } catch (IOException e) {
            e.printStackTrace();
        }
        graphics.drawImage(image, 0, 0, getWidth(), getHeight(), this);
    }

    private void drawImagefromPath(Graphics graphics, Image image) {
//        Toolkit t = Toolkit.getDefaultToolkit();
//        Image image = t.getImage(imagePath);
        graphics.drawImage(image, 0, 0, getWidth(), getHeight(), this);
    }

    private void addImageToCache(URL url) throws IOException {
        String fileName = FilenameUtils.getName(url.getPath());
//        String fileExtn = FilenameUtils.getExtension(url.getPath());
        String fileExtn = "jpg";

        Path imageFile = photoDir.resolve(fileName);

        BufferedImage rawImage = ImageIO.read(url);
        ImageIO.write(rawImage, fileExtn, Files.newOutputStream(imageFile));
    }

    private BufferedImage loadImageFromCache(URL url) {
        String fileName = FilenameUtils.getName(url.getPath());

        Path path = Paths.get(fileName);

        if (Files.exists(path)) {
            try {
                return ImageIO.read(Files.newInputStream(path));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    private boolean imageExistInCache(URL url) {
        String fileName = FilenameUtils.getName(url.getPath());
        Path path = Paths.get(fileName);

        return (Files.exists(path));
    }

}
