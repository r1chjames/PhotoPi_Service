package com.richjames.photopi.exceptions;


public class ErrorSequences {
    public static final Integer AN_UNEXPECTED_ERROR_HAS_OCCURRED = 0;
    public static final Integer CONNECTION_TO_IMAGE_API_FAILED = 3;
    public static final Integer UNABLE_TO_SAVE_IMAGE = 6;
    public static final Integer JSON_PARSING_ERROR = 7;
    public static final Integer JSON_OBJECT_WAS_NOT_WELL_FORMED = 8;
    public static final Integer REQUEST_REQUIRES_CORRECT_AUTH_STRING_BUT_GOT_INCORRECT_AUTH_STRING = 10;
    public static final Integer API_ERROR_RESPONSE = 11;
    public static final Integer NO_RECORD_FOUND = 12;
    public static final Integer CONNECTION_TO_API_TIMEOUT = 13;
    public static final Integer COULD_NOT_PROCESS_JSON_RESPONSE_FROM_API = 14;
    public static final Integer ALREADY_EXISTS = 15;
}
