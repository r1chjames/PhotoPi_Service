package com.richjames.photopi.exceptions;

public class ServiceApplicationException extends RuntimeException {
    private Integer returnCode;
    private Integer errorSequence;

    public ServiceApplicationException(String message) {
        super(message);
    }

    public ServiceApplicationException(Throwable cause) {
        super(cause);
    }

    public ServiceApplicationException(String message, Throwable cause) {
        super(message, cause);
    }

    public ServiceApplicationException(String message, Throwable cause, Integer returnCode, Integer errorSequence) {
        super(message, cause);
        this.returnCode = returnCode;
        this.errorSequence = errorSequence;
    }

    public Integer getReturnCode() {
        return returnCode;
    }

    public Integer getErrorSequence() {
        return errorSequence;
    }
}
