package com.richjames.photopi.exceptions;

public class ApplicationInstanceException extends ServiceApplicationException {
    public ApplicationInstanceException(String message) {
        super(message);
    }

    public ApplicationInstanceException(Throwable cause) {
        super(cause);
    }

    public ApplicationInstanceException(String message, Throwable cause) {
        super(message, cause);
    }

    public ApplicationInstanceException(String message, Throwable cause, Integer returnCode, Integer errorSequence) {
        super(message, cause, returnCode, errorSequence);
    }
}
