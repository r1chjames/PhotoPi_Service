package com.richjames.photopi.model.database;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.mongodb.morphia.annotations.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity("photopi_configuration")
//@Indexes(
//        @Index(value = "salary", fields = @Field("salary"))
//)
public class Configuration {

  @Id
  private String property;
  @Reference
  private String value;

}
