package com.richjames.photopi.model.Px500API;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Photos {

    private int id;
    @JsonProperty("user_id")
    private int userId;
    private String name;
    private String description;
    private String camera;
    private String lens;
    @JsonProperty("focal_length")
    private String focalLength;
    private String iso;
    @JsonProperty("shutter_speed")
    private String shutterSpeed;
    private String aperture;
    @JsonProperty("times_viewed")
    private int timesViewed;
    private double rating;
    private int status;
    @JsonProperty("created_at")
    private Date createdAt;
    private int category;
    private String location;
    private String latitude;
    private String longitude;
    @JsonProperty("taken_at")
    private Date takenAt;
    @JsonProperty("hi_res_uploaded")
    private int hiResUploaded;
    @JsonProperty("for_sale")
    private boolean forSale;
    private int width;
    private int height;
    @JsonProperty("votes_count")
    private int votesCount;
    @JsonProperty("favorites_count")
    private int favoritesCount;
    @JsonProperty("comments_count")
    private int commentsCount;
    private boolean nsfw;
    @JsonProperty("sales_count")
    private int salesCount;
    @JsonProperty("for_sale_date")
    private String forSaleDate;
    @JsonProperty("highest_rating")
    private double highestRating;
    @JsonProperty("highest_rating_date")
    private Date highestRatingDate;
    @JsonProperty("license_type")
    private int licenseType;
    private int converted;
    @JsonProperty("collections_count")
    private int collectionsCount;
    @JsonProperty("crop_version")
    private int cropVersion;
    private boolean privacy;
    private boolean profile;
    @JsonProperty("image_url")
    private String imageUrl;
    private List<Images> images;
    private String url;
    @JsonProperty("positive_votes_count")
    private int positiveVotesCount;
    @JsonProperty("converted_bits")
    private int convertedBits;
    private boolean watermark;
    @JsonProperty("image_format")
    private String imageFormat;
    private User user;
    @JsonProperty("licensing_requested")
    private boolean licensingRequested;
    @JsonProperty("licensing_suggested")
    private boolean licensingSuggested;
    @JsonProperty("is_free_photo")
    private boolean isFreePhoto;

}