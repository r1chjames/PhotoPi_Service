package com.richjames.photopi.model.Px500API;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class Px500Array {

    @JsonProperty("current_page")
    private int currentPage;
    @JsonProperty("total_pages")
    private int totalPages;
    @JsonProperty("total_items")
    private int totalItems;
    private List<Photos> photos;
    private Filters filters;
    private String feature;

}