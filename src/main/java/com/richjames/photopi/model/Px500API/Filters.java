package com.richjames.photopi.model.Px500API;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Filters {

    private boolean category;
    private boolean exclude;

}