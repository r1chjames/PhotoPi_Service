package com.richjames.photopi.model.Px500API;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class Px500Single {

    private Photo photo;
    private List<String> comments;

}