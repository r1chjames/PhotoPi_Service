package com.richjames.photopi.model.Px500API;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class User {

    private int id;
    private String username;
    private String firstname;
    private String lastname;
    private String city;
    private String country;
    private int usertype;
    private String fullname;
    @JsonProperty("userpic_url")
    private String userpicUrl;
    @JsonProperty("userpic_https_url")
    private String userpicHttpsUrl;
    @JsonProperty("cover_url")
    private String coverUrl;
    @JsonProperty("upgrade_status")
    private int upgradeStatus;
    @JsonProperty("store_on")
    private boolean storeOn;
    private int affection;
    @JsonProperty("followers_count")
    private int followersCount;
    private Avatars avatars;
}