package com.richjames.photopi.model.Px500API;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import java.util.Date;
import java.util.List;

@Getter
@Setter
public class Photo {

    private int id;
    @JsonProperty("user_id")
    private int userId;
    private String name;
    private String description;
    private String camera;
    private String lens;
    @JsonProperty("focal_length")
    private String focalLength;
    private String iso;
    @JsonProperty("shutter_speed")
    private String shutterSpeed;
    private String aperture;
    @JsonProperty("times_viewed")
    private int timesViewed;
    private double rating;
    private int status;
    @JsonProperty("created_at")
    private Date createdAt;
    private int category;
    private String location;
    private String latitude;
    private String longitude;
    @JsonProperty("taken_at")
    private Date takenAt;
    @JsonProperty("hi_res_uploaded")
    private int hiResUploaded;
    @JsonProperty("for_sale")
    private boolean forSale;
    private int width;
    private int height;
    @JsonProperty("votes_count")
    private int votesCount;
    @JsonProperty("favorites_count")
    private int favoritesCount;
    @JsonProperty("comments_count")
    private int commentsCount;
    private boolean nsfw;
    @JsonProperty("sales_count")
    private int salesCount;
    @JsonProperty("for_sale_date")
    private String forSaleDate;
    @JsonProperty("highest_rating")
    private double highestRating;
    @JsonProperty("highest_rating_date")
    private Date highestRatingDate;
    @JsonProperty("license_type")
    private int licenseType;
    private boolean converted;
    @JsonProperty("collections_count")
    private int collectionsCount;
    @JsonProperty("crop_version")
    private int cropVersion;
    @JsonProperty("image_format")
    private String imageFormat;
    @JsonProperty("positive_votes_count")
    private int positiveVotesCount;
    private boolean privacy;
    private boolean profile;
    private String url;
    @JsonProperty("image_url")
    private String imageUrl;
    private List<Images> images;
    @JsonProperty("store_download")
    private boolean storeDownload;
    @JsonProperty("store_print")
    private boolean storePrint;
    @JsonProperty("store_license")
    private boolean storeLicense;
    @JsonProperty("request_to_buy_enabled")
    private boolean requestToBuyEnabled;
    @JsonProperty("license_requests_enabled")
    private boolean licenseRequestsEnabled;
    @JsonProperty("store_width")
    private int storeWidth;
    @JsonProperty("store_height")
    private int storeHeight;
    @JsonProperty("converted_bits")
    private int convertedBits;
    @JsonProperty("editors_choice")
    private boolean editorsChoice;
    @JsonProperty("editors_choice_date")
    private String editorsChoiceDate;
    private String feature;
    @JsonProperty("feature_date")
    private Date featureDate;
    @JsonProperty("editored_by")
    private EditoredBy editoredBy;
    private User user;
    private List<String> comments;
    private boolean watermark;
    @JsonProperty("licensing_requested")
    private boolean licensingRequested;
    @JsonProperty("licensing_suggested")
    private boolean licensingSuggested;
    @JsonProperty("is_free_photo")
    private boolean isFreePhoto;
}