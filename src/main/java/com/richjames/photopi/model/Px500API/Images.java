package com.richjames.photopi.model.Px500API;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Images {

    private int size;
    private String url;
    @JsonProperty("https_url")
    private String httpsUrl;
    private String format;

}