package com.richjames.photopi.model.Px500API;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Avatars {

    @JsonProperty("default")
    private Default _default;
    private Large large;
    private Small small;
    private Tiny tiny;

}