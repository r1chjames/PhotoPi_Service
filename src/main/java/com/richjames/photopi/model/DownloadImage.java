package com.richjames.photopi.model;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Rich on 16/04/2017.
 */
@Getter
@Setter
public class DownloadImage {

    private String url;
}
