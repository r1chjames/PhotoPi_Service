package com.richjames.photopi;

import ch.qos.logback.classic.AsyncAppender;
import ch.qos.logback.classic.Logger;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.richjames.photopi.config.ApplicationConfiguration;
import com.richjames.photopi.filters.GeneralExceptionMapper;
import com.richjames.photopi.filters.ServiceApplicationExceptionMapper;
import com.richjames.photopi.gui.GuiRun;
import com.richjames.photopi.healthcheck.AppHealthCheck;
import com.richjames.photopi.resources.ConfigurationResource;
import com.richjames.photopi.resources.HealthCheckResource;
import com.richjames.photopi.resources.FlickrResource;
import com.richjames.photopi.resources.Px500Resource;
import de.thomaskrille.dropwizard_template_config.TemplateConfigBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.swagger.config.ScannerFactory;
import io.swagger.jaxrs.config.DefaultJaxrsScanner;
import io.swagger.jaxrs.listing.SwaggerSerializers;
import io.swagger.jersey.listing.ApiListingResourceJSON;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.slf4j.LoggerFactory;
import ru.vyarus.dropwizard.guice.GuiceBundle;
import ru.vyarus.dropwizard.guice.module.installer.feature.jersey.ResourceInstaller;

public class Application extends io.dropwizard.Application<ApplicationConfiguration> {
  private static Environment environment;

  public static void main(String[] args) throws Exception {
    Application application = new Application();
    application.run(args);
    GuiRun.main();
  }

  public static void stop() throws Exception {
    environment.getApplicationContext().getServer().stop();
  }

  @Override
  public void initialize(Bootstrap<ApplicationConfiguration> bootstrap) {

    GuiceBundle<ApplicationConfiguration> guiceBundle = GuiceBundle.<ApplicationConfiguration>builder()
      .installers(ResourceInstaller.class)
      .extensions(FlickrResource.class, HealthCheckResource.class, Px500Resource.class, ConfigurationResource.class)
      .build();
    bootstrap.addBundle(guiceBundle);
    bootstrap.addBundle(new TemplateConfigBundle());

  }

  @Override
  public void run(ApplicationConfiguration applicationConfiguration, Environment environment) {
    this.environment = environment;
    environment.healthChecks().register("appHealthcheck", new AppHealthCheck());
    environment.jersey().register(FlickrResource.class);
    environment.jersey().register(Px500Resource.class);
    environment.jersey().register(ConfigurationResource.class);
    environment.jersey().register(HealthCheckResource.class);
    environment.jersey().register(MultiPartFeature.class);
    environment.jersey().register(new GeneralExceptionMapper());
    environment.jersey().register(new ServiceApplicationExceptionMapper());
    //environment.jersey().register(InternalBasicAuthenticationFilter.class); //Commented out unless security required
    environment.jersey().register(ApiListingResourceJSON.class);
    environment.jersey().register(SwaggerSerializers.class);
    environment.getObjectMapper().enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
    environment.getObjectMapper().disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
    ScannerFactory.setScanner(new DefaultJaxrsScanner());
    Logger root = (Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
    AsyncAppender appender = (AsyncAppender) root.getAppender("async-console-appender");
    appender.setIncludeCallerData(true);
  }

}
