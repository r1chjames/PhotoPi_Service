package com.richjames.photopi.dao;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.richjames.photopi.config.ApplicationConfiguration;
import com.richjames.photopi.exceptions.ApplicationInstanceException;
import com.richjames.photopi.exceptions.ErrorSequences;
import com.richjames.photopi.model.Px500API.Px500Array;
import com.richjames.photopi.model.Px500API.Px500Single;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicHeader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.time.Instant;

import static com.richjames.photopi.dao.Px500UrlConstants.POPULAR_IMAGES_JSON;
import static com.richjames.photopi.dao.Px500UrlConstants.SPECIFIC_IMAGE_JSON;
import static com.richjames.photopi.utils.LogFormatters.LOG_ACTION_TIME;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static org.apache.http.protocol.HTTP.CONTENT_TYPE;
import static org.apache.http.protocol.HTTP.USER_AGENT;

/**
 * Created by Rich on 06/12/2016.
 */
public class Px500APIDao {

    private static final Logger log = LoggerFactory.getLogger(Px500APIDao.class);

    ApplicationConfiguration applicationConfiguration;

    @Inject
    public Px500APIDao(ApplicationConfiguration applicationConfiguration) {
        this.applicationConfiguration = applicationConfiguration;
    }

    public Px500Array getPopularImagesJson() {
        long startMillis = System.currentTimeMillis();
        URIBuilder uriBuilder = new URIBuilder()
                .setScheme("https")
                .setHost(String.format(applicationConfiguration.getPx500API().getServiceEndpoint(), POPULAR_IMAGES_JSON))
                .addParameter("consumer_key", applicationConfiguration.getPx500API().getApikey())
                .addParameter("feature", "popular")
                .addParameter("image_size", "2048");

        final HttpGet request = HttpDao.getHttpRequest(uriBuilder.toString());
        final HttpResponse response = HttpDao.httpCall(request);
        Px500Array jsonResponse;

        if (response.getStatusLine().getStatusCode() != HttpStatus.SC_ACCEPTED && response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
            log.error("Unable to connect to Card API");
            throw new ApplicationInstanceException("Unable to connect to Image API", null, 404, ErrorSequences.CONNECTION_TO_IMAGE_API_FAILED);
        }
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            jsonResponse = objectMapper.readValue(response.getEntity().getContent(), Px500Array.class);
        } catch (Exception e) {
            log.error(String.valueOf(e));
            throw new ApplicationInstanceException("Unable to connect to Image API", null, 404, ErrorSequences.CONNECTION_TO_IMAGE_API_FAILED);
        }

        log.info(String.format(LOG_ACTION_TIME, "LOG-API-CALL", (System.currentTimeMillis() - startMillis)));

        return jsonResponse;
    }

    public Px500Single getSpecificImageJson(String imageId) {
        long startMillis = System.currentTimeMillis();

        URIBuilder uriBuilder = new URIBuilder()
                .setScheme("https")
                .setHost(String.format(applicationConfiguration.getPx500API().getServiceEndpoint(), String.format(SPECIFIC_IMAGE_JSON, imageId)))
                .addParameter("consumer_key", applicationConfiguration.getPx500API().getApikey())
                .addParameter("image_size", "2048");

        final HttpGet request = HttpDao.getHttpRequest(uriBuilder.toString());
        final HttpResponse response = HttpDao.httpCall(request);
        Px500Single jsonResponse;

        if (response.getStatusLine().getStatusCode() != HttpStatus.SC_ACCEPTED && response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
            log.error("Unable to connect to Card API");
            throw new ApplicationInstanceException("Unable to connect to Image API", null, 404, ErrorSequences.CONNECTION_TO_IMAGE_API_FAILED);
        }
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            jsonResponse = objectMapper.readValue(response.getEntity().getContent(), Px500Single.class);
        } catch (Exception e) {
            log.error(String.valueOf(e));
            throw new ApplicationInstanceException("Unable to connect to Image API", null, 404, ErrorSequences.CONNECTION_TO_IMAGE_API_FAILED);
        }

        log.info(String.format(LOG_ACTION_TIME, "LOG-API-CALL", (System.currentTimeMillis() - startMillis)));

        return jsonResponse;
    }

    public String downloadImage(String url, String imageName) {
        long startMillis = System.currentTimeMillis();

        if (imageName == null){
            imageName = Instant.now().toString();
        }

        final HttpGet request = HttpDao.getHttpRequest(url);
        final HttpResponse response = HttpDao.httpCall(request);

        if (response.getStatusLine().getStatusCode() != HttpStatus.SC_ACCEPTED && response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
            log.error("Unable to connect to Card API");
            throw new ApplicationInstanceException("Unable to connect to Image API", null, 404, ErrorSequences.CONNECTION_TO_IMAGE_API_FAILED);
        }
        try {
            BufferedImage rawImage = ImageIO.read(response.getEntity().getContent());
            File image = new File(String.format(applicationConfiguration.getPx500API().getDownloadLocation(), imageName));
            ImageIO.write(rawImage, "jpg", image);
        } catch (Exception e) {
            log.error(String.valueOf(e));
            throw new ApplicationInstanceException("Unable to write image", null, 404, ErrorSequences.UNABLE_TO_SAVE_IMAGE);
        }
        log.info(String.format(LOG_ACTION_TIME, "LOG-API-CALL", (System.currentTimeMillis() - startMillis)));
        return applicationConfiguration.getPx500API().getDownloadLocation();
    }

}
