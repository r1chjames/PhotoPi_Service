package com.richjames.photopi.dao;

import com.richjames.photopi.exceptions.ApplicationInstanceException;
import com.richjames.photopi.exceptions.ErrorSequences;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicHeader;

import java.io.IOException;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static org.apache.http.protocol.HTTP.CONTENT_TYPE;
import static org.apache.http.protocol.HTTP.USER_AGENT;

/**
 * Created by Rich on 06/12/2016.
 */
public class HttpDao {

    private static Header[] setHeaders() {
        return new Header[]{
                new BasicHeader(USER_AGENT,""),
                new BasicHeader(CONTENT_TYPE, APPLICATION_JSON)};
    }

    public static HttpGet getHttpRequest(String uri) {
        HttpGet request = new HttpGet(uri);
        request.setHeaders(setHeaders());
        return request;
    }

    public static HttpResponse httpCall(HttpUriRequest request) {
        final HttpClient httpClient = HttpClientBuilder.create().build();
        try {
            return httpClient.execute(request);
        } catch (IOException e) {
            throw new ApplicationInstanceException(
                    "Failed to connect to endpoint", e, 500, ErrorSequences.CONNECTION_TO_IMAGE_API_FAILED);
        }
    }

}
