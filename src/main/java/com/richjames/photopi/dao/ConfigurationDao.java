package com.richjames.photopi.dao;

import com.google.inject.Inject;
import com.richjames.photopi.config.ApplicationConfiguration;
import com.richjames.photopi.model.database.Configuration;
import org.mongodb.morphia.Datastore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by Rich on 06/12/2016.
 */
public class ConfigurationDao {

    private static final Logger log = LoggerFactory.getLogger(ConfigurationDao.class);

    ApplicationConfiguration applicationConfiguration;
    MongoDao mongoDao;

    @Inject
    public ConfigurationDao(ApplicationConfiguration applicationConfiguration, MongoDao mongoDao) {
        this.applicationConfiguration = applicationConfiguration;
        this.mongoDao = mongoDao;
    }

    public List<Configuration> getAllConfiguration() {
        Datastore datastore = mongoDao.initMorphia();
        return datastore
                .createQuery(Configuration.class)
                .asList();
    }

    public List<Configuration> getConfiguration(String queryProperty) {
        Datastore datastore = mongoDao.initMorphia();
        return datastore
                .createQuery(Configuration.class)
                .field("property").equal(queryProperty)
                .asList();
    }

    public Configuration setConfiguration(Configuration configuration) {
        Datastore datastore = mongoDao.initMorphia();
        datastore.save(configuration);
        return configuration;
    }
}
