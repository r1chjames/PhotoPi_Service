package com.richjames.photopi.dao;

import com.google.inject.Inject;
import com.richjames.photopi.config.ApplicationConfiguration;
import com.richjames.photopi.exceptions.ErrorSequences;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.richjames.photopi.utils.LogFormatters.LOG_ACTION_TIME;
import static org.apache.http.protocol.HTTP.USER_AGENT;

/**
 * Created by Rich on 06/12/2016.
 */
public class FlickrAPIDao {

    private static final Logger log = LoggerFactory.getLogger(FlickrAPIDao.class);

    ApplicationConfiguration applicationConfiguration;

    HttpClient httpClient;

    @Inject
    public FlickrAPIDao(ApplicationConfiguration applicationConfiguration) {
        this.applicationConfiguration = applicationConfiguration;
    }

    public String getMoreCard(String moreCard) {
        long startMillis = System.currentTimeMillis();

        String path = String.format("%s/balance", moreCard);

        HttpGet request = new HttpGet(String.format(applicationConfiguration.getFlickrAPI().getServiceEndpoint(), path, applicationConfiguration.getFlickrAPI().getApikey()));

        httpClient = HttpClientBuilder.create().build();

        request.addHeader("User-Agent", USER_AGENT);
        request.addHeader("Content-type", "application/json");

        String stringResponse = "";
        HttpResponse response;

        try {
            response = httpClient.execute(request);
            if (response.getStatusLine().getStatusCode() != HttpStatus.SC_ACCEPTED && response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
                log.error("Unable to connect to Card API");
                throw new com.richjames.photopi.exceptions.ApplicationInstanceException("Unable to connect to Image API", null, 404, ErrorSequences.CONNECTION_TO_IMAGE_API_FAILED);
            }
            stringResponse = IOUtils.toString(response.getEntity().getContent(), "UTF-8");
        } catch (Exception e) {
            log.error(String.valueOf(e));
            throw new com.richjames.photopi.exceptions.ApplicationInstanceException("Unable to connect to Image API", null, 404, ErrorSequences.CONNECTION_TO_IMAGE_API_FAILED);
        }

        log.info(String.format(LOG_ACTION_TIME, "LOG-MORE-CARD-ENQUIRY", (System.currentTimeMillis() - startMillis)));

        return stringResponse;
    }
}
