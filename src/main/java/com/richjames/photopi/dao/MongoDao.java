package com.richjames.photopi.dao;

import com.google.inject.Inject;
import com.mongodb.MongoClient;
import com.richjames.photopi.config.ApplicationConfiguration;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;

/**
 * Created by Rich on 16/04/2017.
 */
public class MongoDao {

    ApplicationConfiguration applicationConfiguration;

    @Inject
    public MongoDao(ApplicationConfiguration applicationConfiguration) {
        this.applicationConfiguration = applicationConfiguration;
    }

    public Datastore initMorphia() {

        Morphia morphia = new Morphia();

        morphia.mapPackage("org.richjames.model.database");

        String host = applicationConfiguration.getMongo().getHost();
        int port = applicationConfiguration.getMongo().getPort();

        final Datastore datastore = morphia.createDatastore(new MongoClient(host, port), "photopi");
        datastore.ensureIndexes();

        return datastore;
    }

}
