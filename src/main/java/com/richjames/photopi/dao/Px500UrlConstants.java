package com.richjames.photopi.dao;

/**
 * Created by peter on 01/11/2016.
 */
public class Px500UrlConstants {

    public static final String POPULAR_IMAGES_JSON = "photos/";
    public static final String SPECIFIC_IMAGE_JSON = "photos/%s";
    public static final String DOWNLOAD_IMAGE = "photos/%s";

}
