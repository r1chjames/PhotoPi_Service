package com.richjames.photopi.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class APISuccessResponseDTO {
    private Integer httpResponseCode;

    public APISuccessResponseDTO(Integer httpResponseCode) {
        this.httpResponseCode = httpResponseCode;
    }
}
