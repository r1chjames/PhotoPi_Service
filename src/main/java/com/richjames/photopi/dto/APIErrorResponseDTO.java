package com.richjames.photopi.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class APIErrorResponseDTO {
    private Integer httpResponseCode;
    private String errorCode;
    private String errorMessage;
    private String errorMoreInfo;

    public APIErrorResponseDTO(Integer httpResponseCode, String errorCode, String errorMessage) {
        this.httpResponseCode = httpResponseCode;
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
        this.errorMoreInfo = "";
    }
}
