package com.richjames.photopi.resources;

import com.codahale.metrics.annotation.Timed;
import com.google.inject.Inject;
import com.richjames.photopi.dao.ConfigurationDao;
import com.richjames.photopi.model.database.Configuration;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static com.richjames.photopi.utils.HttpUtil.withExceptionHandler;


@Path("/configuration")
@Api(value = "configuration", description = "Provides Operations for PhotoPi Angular.js front end")
@SwaggerDefinition(basePath = "/photopi/v1",
                info = @Info(
                        title = "PhotoPi API",
                        description = "PhotoPi Service",
                        version = "1.0.0")
)
@Produces(MediaType.APPLICATION_JSON)
public class ConfigurationResource {

    private static final Logger log = LoggerFactory.getLogger(ConfigurationResource.class);

    private ConfigurationDao configurationDao;

    @Inject
    public ConfigurationResource(ConfigurationDao configurationDao) {
        this.configurationDao = configurationDao;
    }

    @Timed
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    @Path("/")
    @ApiOperation(value = "Enumeration")
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "Resource ID not found"),
            @ApiResponse(code = 400, message = "Invalid resource spec")})
    public Response getAllConfiguration(@Context HttpHeaders headers) {

        return withExceptionHandler(() ->  Response.ok(configurationDao.getAllConfiguration()).build());
    }

    @Timed
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    @Path("/{queryProperty}")
    @ApiOperation(value = "Enumeration")
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "Resource ID not found"),
            @ApiResponse(code = 400, message = "Invalid resource spec")})
    public Response getImageJson(@Context HttpHeaders headers,
                                 @ApiParam(value = "Query Property", required = true) @PathParam("queryProperty") String queryProperty
                                 ) {

        return withExceptionHandler(() ->  Response.ok(configurationDao.getConfiguration(queryProperty)).build());
    }

    @Timed
    @POST
    @Path("/")
    @ApiOperation(value = "Enumeration")
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "Resource ID not found"),
            @ApiResponse(code = 400, message = "Invalid resource spec")})
    public Response downloadImage(@Context HttpHeaders headers,
                                 @ApiParam(value = "Image ID", required = true) Configuration configuration
                                ) {

        return withExceptionHandler(() ->  Response.ok(configurationDao.setConfiguration(configuration)).build());
    }

}
