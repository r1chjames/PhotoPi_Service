package com.richjames.photopi.resources;

import com.codahale.metrics.annotation.Timed;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.google.inject.Inject;
import com.richjames.photopi.dao.Px500APIDao;
import com.richjames.photopi.model.DownloadImage;
import com.richjames.photopi.model.Px500API.Px500Single;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static com.richjames.photopi.utils.HttpUtil.withExceptionHandler;


@Path("/500px")
@Api(value = "500px", description = "Provides Operations for PhotoPi Angular.js front end")
@SwaggerDefinition(basePath = "/photopi/v1",
                info = @Info(
                        title = "PhotoPi API",
                        description = "PhotoPi Service",
                        version = "1.0.0")
)
@Produces(MediaType.APPLICATION_JSON)
public class Px500Resource {

    private static final Logger log = LoggerFactory.getLogger(Px500Resource.class);

    private Px500APIDao px500APIDao;

    @Inject
    public Px500Resource(Px500APIDao px500APIDao) {
        this.px500APIDao = px500APIDao;
    }

    @Timed
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    @Path("/json/popular")
    @ApiOperation(value = "Enumeration")
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "Resource ID not found"),
            @ApiResponse(code = 400, message = "Invalid resource spec")})
    public Response getPopularJson(@Context HttpHeaders headers) {

        return withExceptionHandler(() ->  Response.ok(px500APIDao.getPopularImagesJson()).build());
    }

    @Timed
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    @Path("/json/id/{id}")
    @ApiOperation(value = "Enumeration")
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "Resource ID not found"),
            @ApiResponse(code = 400, message = "Invalid resource spec")})
    public Response getImageJson(@Context HttpHeaders headers,
                                 @ApiParam(value = "Image ID", required = true) @PathParam("id") String imageId
                                 ) {

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        return withExceptionHandler(() -> {
            try {
                return Response.ok(objectMapper.writeValueAsString(px500APIDao.getSpecificImageJson(imageId))).build();
            } catch (JsonProcessingException e) {
                e.printStackTrace();
                return null;
            }
        });
    }

    @Timed
    @POST
    @Path("/download/id/{id}")
    @ApiOperation(value = "Enumeration")
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "Resource ID not found"),
            @ApiResponse(code = 400, message = "Invalid resource spec")})
    public Response downloadImage(@Context HttpHeaders headers,
                                  @ApiParam(value = "Image ID", required = true) @PathParam("id") String imageId
    ) {

        Px500Single single = px500APIDao.getSpecificImageJson(imageId);
        return withExceptionHandler(() ->  Response.accepted(px500APIDao.downloadImage(
                single.getPhoto().getImageUrl(),
                single.getPhoto().getName().replace(" ", "_"))).build());
    }

    @Timed
    @POST
    @Path("/download/url")
    @ApiOperation(value = "Enumeration")
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "Resource ID not found"),
            @ApiResponse(code = 400, message = "Invalid resource spec")})
    public Response downloadImage(@Context HttpHeaders headers,
                                 @ApiParam(value = "Image ID", required = true) DownloadImage downloadImage
    ) {

        return withExceptionHandler(() ->  Response.accepted(px500APIDao.downloadImage(downloadImage.getUrl(), null)).build());
    }

    @Timed
    @POST
    @Path("/download/popular")
    @ApiOperation(value = "Enumeration")
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "Resource ID not found"),
            @ApiResponse(code = 400, message = "Invalid resource spec")})
    public Response downloadPopularImages(@Context HttpHeaders headers,
                                 @ApiParam(value = "Image ID", required = true) DownloadImage downloadImage
    ) {

        return withExceptionHandler(() ->  Response.accepted(px500APIDao.downloadImage(downloadImage.getUrl(), null)).build());
    }

}
