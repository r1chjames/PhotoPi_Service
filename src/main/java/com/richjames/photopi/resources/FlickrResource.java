package com.richjames.photopi.resources;

import com.codahale.metrics.annotation.Timed;
import com.google.inject.Inject;
import com.richjames.photopi.dao.FlickrAPIDao;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.*;

import static com.richjames.photopi.utils.HttpUtil.withExceptionHandler;


@Path("/flickr")
@Api(value = "Flickr", description = "Provides Operations for PhotoPi Angular.js front end")
@SwaggerDefinition(basePath = "/photopi/v1",
                info = @Info(
                        title = "PhotoPi API",
                        description = "PhotoPi Service",
                        version = "1.0.0")
)
@Produces(MediaType.APPLICATION_JSON)
public class FlickrResource {

    private static final Logger log = LoggerFactory.getLogger(FlickrResource.class);

    private FlickrAPIDao flickrAPIDao;

    @Inject
    public FlickrResource(FlickrAPIDao flickrAPIDao) {
        this.flickrAPIDao = flickrAPIDao;
    }

    @Timed
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    @Path("/")
    @ApiOperation(value = "Enumeration")
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "Resource ID not found"),
            @ApiResponse(code = 400, message = "Invalid resource spec")})
    public Response getAllSchemes(@Context HttpHeaders headers) {

        return withExceptionHandler(() ->  Response.ok(flickrAPIDao.getMoreCard("")).build());
    }

}
