package com.richjames.photopi.resources;

import com.codahale.metrics.annotation.Timed;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


@Path("/healthcheck")

@Produces(MediaType.APPLICATION_JSON)
public class HealthCheckResource {

    private static final Logger log = LoggerFactory.getLogger(HealthCheckResource.class);

    @GET
    @Timed
    @ApiOperation(value = "Health Check", notes = "Reports app status")
    @Produces(MediaType.APPLICATION_JSON)
    @Path(value = "/health")
    public Response ping() {
        return Response.status(200).entity("Application is running...").build();
    }

}
