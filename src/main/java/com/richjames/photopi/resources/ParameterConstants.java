package com.richjames.photopi.resources;

/**
 * Created by peter on 28/11/2016.
 */
public class ParameterConstants {

    public static final String ACCOUNT_ID = "accountId";
    public static final String CORRELATION_TOKEN = "correlationToken";
}
