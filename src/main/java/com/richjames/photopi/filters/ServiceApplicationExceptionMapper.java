package com.richjames.photopi.filters;

/**
 * Created by sysdevan on 05/08/2016.
 */

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.richjames.photopi.exceptions.ErrorSequences;
import com.richjames.photopi.exceptions.ServiceApplicationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import static org.apache.commons.lang3.StringUtils.leftPad;

@Provider
public class ServiceApplicationExceptionMapper implements ExceptionMapper<ServiceApplicationException> {
    public static final Logger logger = LoggerFactory.getLogger(ServiceApplicationExceptionMapper.class);
    public static final String API_INDEX = "21";

    @Context
    private HttpHeaders headers;

    @Override
    public Response toResponse(com.richjames.photopi.exceptions.ServiceApplicationException e) {
        Integer returnCode = e.getReturnCode();
        Integer errorSequence = e.getErrorSequence();
        String errorCode = String.format("%s.%s",
                leftPad(String.valueOf(returnCode), 3, '0'),
                leftPad(String.valueOf(API_INDEX), 2, '0'),
                leftPad(String.valueOf(errorSequence), 3, '0'));
        String errorMessage = e.getMessage();
        com.richjames.photopi.dto.APIErrorResponseDTO morrisonsErrorResponse = new com.richjames.photopi.dto.APIErrorResponseDTO(returnCode, errorCode, errorMessage);
        try {
            logger.error(new ObjectMapper().writeValueAsString(morrisonsErrorResponse));
        } catch (JsonProcessingException ex) {
            throw new com.richjames.photopi.exceptions.ApplicationInstanceException("A JSON parsing error occurred", e, 400, ErrorSequences.JSON_PARSING_ERROR);
        }

        Response.ResponseBuilder responseBuilder = Response
                .status(returnCode)
                .entity(morrisonsErrorResponse)
                .type(headers.getMediaType());

        return responseBuilder.build();
    }
}
