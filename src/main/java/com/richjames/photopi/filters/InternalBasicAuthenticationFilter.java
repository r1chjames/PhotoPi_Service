package com.richjames.photopi.filters;

import com.richjames.photopi.exceptions.ErrorSequences;
import com.richjames.photopi.security.Authenticated;
import org.apache.commons.lang3.StringUtils;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.util.Optional;

@Provider
public class InternalBasicAuthenticationFilter implements ContainerRequestFilter {
    @Context
    private ResourceInfo resourceInfo;

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        Optional<String> expectedAuthString = Optional.ofNullable(resourceInfo.getResourceClass().getAnnotation(Authenticated.class).value());
        Optional<String> requestAuthString = Optional.ofNullable(StringUtils.removeStart(requestContext.getHeaderString("Authorization"), "Basic "));

        expectedAuthString.ifPresent(expected -> {
            if (!requestAuthString.isPresent() || !requestAuthString.get().equals(expectedAuthString.get())) {
                throw new com.richjames.photopi.exceptions.ServiceApplicationException(
                        "The request client is not authorised to perform this action with the auth-string provided",
                        null, 401, ErrorSequences.REQUEST_REQUIRES_CORRECT_AUTH_STRING_BUT_GOT_INCORRECT_AUTH_STRING);
            }
        });
    }
}
