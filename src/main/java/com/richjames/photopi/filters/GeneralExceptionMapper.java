package com.richjames.photopi.filters;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.richjames.photopi.exceptions.ErrorSequences;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import static org.apache.commons.lang3.StringUtils.leftPad;

@Provider
public class GeneralExceptionMapper implements ExceptionMapper<RuntimeException> {
    public static final Logger logger = LoggerFactory.getLogger(GeneralExceptionMapper.class);

    @Context
    private HttpHeaders headers;

    @Override
    public Response toResponse(RuntimeException e) {
        Integer returnCode = Response.Status.INTERNAL_SERVER_ERROR.getStatusCode();
        Integer errorSequence = ErrorSequences.AN_UNEXPECTED_ERROR_HAS_OCCURRED;
        String errorCode = String.format("%s.%s",
                leftPad(String.valueOf(returnCode), 3, '0'),
                leftPad(String.valueOf(errorSequence), 3, '0'));
        String errorMessage = String.format("An unexpected error has occurred - %s", e.getMessage());
        com.richjames.photopi.dto.APIErrorResponseDTO morrisonsErrorResponse = new com.richjames.photopi.dto.APIErrorResponseDTO(returnCode, errorCode, errorMessage);
        try {
            logger.error(new ObjectMapper().writeValueAsString(morrisonsErrorResponse));
            logger.error("The stack trace is: ", e);
        } catch (JsonProcessingException ex) {
            throw new com.richjames.photopi.exceptions.ApplicationInstanceException("A JSON parsing error occurred", e, 400, ErrorSequences.JSON_PARSING_ERROR);
        }

        Response.ResponseBuilder responseBuilder = Response
                .status(returnCode)
                .entity(morrisonsErrorResponse)
                .type(headers.getMediaType());

        return responseBuilder.build();
    }
}
