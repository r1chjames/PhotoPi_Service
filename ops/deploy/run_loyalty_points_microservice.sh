#!/bin/sh
# Hook script for Loyalty Points microservice.
# Pass in environment string which can be 'DEV', 'SIT', 'UAT' ,'PROD'
environment="$1"
listening_port="-1"

if [ "$environment" = "DEV" ] ; then
    listening_port=8090
elif [ "$environment" = "SIT" ] ; then
    listening_port=10010
elif [ "$environment" = "UAT" ] ; then
    listening_port=10015
#elif [ "$environment" = "PROD" ] ; then
#    listening_port=10095
else
    echo 'You must specify an environment as the 1st argument!'
    echo 'Usage: run_loyalty_points_microservice.sh <DEV|SIT|UAT|PROD>'
fi

echo "Environment = ${environment}"
echo "Listening port = ${listening_port}"

docker run -d --restart=always -v "/app/mus/loyaltypointsservice/logs/${environment}":/app/mus/loyaltypointsservice/logs -p "8080:${listening_port}" -p 8081:8081 --name="loyalty_points_service_${environment}" "morrisons/loyalty-points:${environment}" "app/mus/loyaltypointsservice/bin/runApp" "/app/mus/loyaltypointsservice/config/application_${environment}.yml"