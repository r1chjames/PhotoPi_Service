#!/bin/bash

env=$1
imagename=$2
jenkinsawsuser=$3

if [[ ${env} == "dev" ]]; then
	eval $(aws ecr get-login --region eu-west-1 --profile ${jenkinsawsuser} | sed 's|https://||')
	docker build -t ${imagename}:${env} .
    docker push ${imagename}:${env}

elif [[ ${env} == "cit" ]]; then
	eval $(aws ecr get-login --region eu-west-1 --profile ${jenkinsawsuser} | sed 's|https://||')
    docker pull ${imagename}:dev
    docker tag ${imagename}:dev ${imagename}:cit
    docker push ${imagename}:cit

elif [[ ${env} == "sit" ]]; then
	eval $(aws ecr get-login --region eu-west-1 --profile ${jenkinsawsuser} | sed 's|https://||')
    docker pull ${imagename}:cit
    docker tag ${imagename}:cit ${imagename}:sit
    docker push ${imagename}:sit

elif [[ ${env} == "uat" ]]; then
	eval $(aws ecr get-login --region eu-west-1 --profile ${jenkinsawsuser} | sed 's|https://||')
	docker pull ${imagename}:sit
    docker tag ${imagename}:sit ${imagename}:uat
    docker push ${imagename}:uat

elif [[ ${env} == "pre" ]]; then
    eval $(aws ecr get-login --region eu-west-1 --profile ${jenkinsawsuser} | sed 's|https://||')
    docker pull ${imagename}:uat
    docker tag ${imagename}:uat ${imagename}:pre
    docker push ${imagename}:pre

elif [[ ${env} == "prod" ]]; then
	eval $(aws ecr get-login --region eu-west-1 --profile ${jenkinsawsuser} | sed 's|https://||')
	docker pull ${imagename}:prod
	docker tag ${imagename}:prod ${imagename}:prod-backup
	docker push ${imagename}:prod-backup

	docker pull ${imagename}:pre
	docker tag ${imagename}:pre ${imagename}:prod
    docker push ${imagename}:prod
else
    exit
fi

