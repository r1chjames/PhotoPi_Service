#!/bin/bash

env=$1
imagename=$2
jenkinsawsuser=$3

if [[ ${env} == "prod" ]]; then
    images_to_delete=$( aws ecr list-images --repository-name ${imagename} --filter "tagStatus=UNTAGGED" --query 'imageIds[*]' --output json --profile ${jenkinsawsuser} )
	aws ecr batch-delete-image --repository-name ${imagename} --image-ids "$images_to_delete" --profile ${jenkinsawsuser} || true
elif [[ ${env} == "dev" || ${env} == "cit" || ${env} == "sit" || ${env} == "uat" || ${env} == "pre" ]]; then
    images_to_delete=$( aws ecr list-images --repository-name ${imagename} --filter "tagStatus=UNTAGGED" --query 'imageIds[*]' --output json --profile ${jenkinsawsuser} )
	aws ecr batch-delete-image --repository-name ${imagename} --image-ids "$images_to_delete" --profile ${jenkinsawsuser} || true
else
    exit
fi