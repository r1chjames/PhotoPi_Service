#!/bin/bash

env=$1
imagename=$2
jenkinsawsuser=$3


eval $(aws ecr get-login --region eu-west-1 --profile ${jenkinsawsuser} | sed 's|https://||')

aws ecs register-task-definition --cli-input-json file://create-ecs-task-${env}.json --profile ${jenkinsawsuser}

aws ecs create-service --cli-input-json file://create-ecs-service-${env}.json --profile ${jenkinsawsuser} || true

aws ecs update-service --cli-input-json file://update-ecs-service-${env}.json --profile ${jenkinsawsuser}